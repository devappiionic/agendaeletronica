import React, { Component } from 'react';
import Contato from './contato/Contato';
import { Table } from 'react-bootstrap';

const contatos = (props) => (
    <Table striped bordered hover>
        <thead>
            <tr>
                <th>
                    ID
                </th>
                <th>
                    Nome
                </th>
            </tr>
        </thead>        
        <tbody>
        {Object.keys(props.lista)
            .map((dados) => {
            return (
                <Contato contato={props.lista[dados]} key={dados} />
            )
        })}
        </tbody>        
    </Table>
);

export default contatos;