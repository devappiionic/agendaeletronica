import React, { Component } from 'react';
import Auxi from '../../../helper/ui/Auxi';
import { Card, Form, Button, Col } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

const contatoForm = (props) => {
    const contato = props.contato;
    const { validated } = props;    
    
    return (        
        <Auxi>
            <Form 
                noValidate 
                onSubmit={props.onSubmitForm}
                validated={validated}
                method='post'
                >
                <Form.Control type='hidden' name='id' value={contato.id} onChange={props.onChangeCampo}/>
                <Card.Title>Edição de Cadastro</Card.Title>
                <Card.Body>
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label>
                                Nome
                                <Form.Control type='text' name='name' required value={contato.name} onChange={props.onChangeCampo}/>
                                <Form.Control.Feedback type="invalid">
                                    Campo Obrigatório.
                                </Form.Control.Feedback>
                            </Form.Label>
                        </Form.Group>     
                        <Form.Group as={Col}>
                            <Form.Label>
                                Sobrenome
                                <Form.Control type='text' name='sobrenome' required value={contato.sobrenome} onChange={props.onChangeCampo}/>
                                <Form.Control.Feedback type="invalid">
                                    Campo Obrigatório.
                                </Form.Control.Feedback>
                            </Form.Label>
                        </Form.Group>     
                    </Form.Row>                   
                    <Form.Row>
                        <Form.Group as={Col}>
                            <Form.Label>
                                DDD
                            </Form.Label>
                            <Form.Control type='phone' name='ddd' required value={contato.ddd} onChange={props.onChangeCampo}/>
                            <Form.Control.Feedback type="invalid">
                                Campo Obrigatório.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>
                                Telefone
                            </Form.Label>
                            <Form.Control type='phone' name='telefone' value={contato.telefone} required onChange={props.onChangeCampo}/>
                            <Form.Control.Feedback type="invalid">
                                Campo Obrigatório.
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>                        
                        <Form.Group as={Col}>
                            <Button type='submit'>Confirmar</Button>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Button type='button' onClick={() => props.history.goBack()}>Voltar</Button>
                        </Form.Group>
                    </Form.Row>
                </Card.Body>
            </Form>
        </Auxi>
    )
}

export default withRouter(contatoForm);