import React, { Component } from 'react';
import './contato.css';
import { Link } from 'react-router-dom';

const contato = (props) => (    
    <tr className="contato">
        <td>
            <Link to={{pathname: '/edicao', params: props.contato.id}}>
                {props.contato.id}
            </Link>
        </td>
        <td>
            <Link to={{pathname: '/edicao', params: props.contato.id}}>
                {props.contato.name}
            </Link>
        </td>
    </tr>
)

export default contato;