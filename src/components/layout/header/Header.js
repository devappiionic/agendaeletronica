import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

const header = (props) => (
    <Card.Header>
        <Card.Title>Agenda Eletrônica</Card.Title>
    </Card.Header>    
)

export default header;