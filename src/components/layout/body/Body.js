import React, { Component } from 'react';
import { Card } from 'react-bootstrap';

const body = (props) => (
    <Card.Body>
        {props.children}
    </Card.Body>    
)

export default body;