import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import Body from './body/Body';
import Header from './header/Header';
import Footer from './footer/Footer';
import Navbar from './navbar/Navbar';

const layout = (props) => (
    <Card>
        <Header props={props} />
        <Navbar props={props} />
        <Body props={props}>
            {props.children}
        </Body>
        <Footer props={props} />
    </Card>
)

export default layout;