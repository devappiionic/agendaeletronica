import React, { Component } from 'react';
import { Nav } from 'react-bootstrap';

const navbar = (props) => (
    <Nav className="justify-content-center">
        <Nav.Item>
            <Nav.Link href="/home" activeKey="/home">Home</Nav.Link>
        </Nav.Item>        
        <Nav.Item>
            <Nav.Link href="/edicao" activeKey="/edicao">Cadastro</Nav.Link>
        </Nav.Item>        
        <Nav.Item>
            <Nav.Link href="/listagem" activeKey="/listagem">Listagem</Nav.Link>
        </Nav.Item>
    </Nav>     
)

export default navbar;