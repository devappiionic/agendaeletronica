import React from 'react';
import { Modal } from 'react-bootstrap';

class MeuModal extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            smShow: true,
            lgShow: false,
        };
    }

    smClose = () => {        
        this.setState({ smShow: false });
        this.props.onHideModal();
    }

    render(){
        
        return (
            <Modal
            size="sm"
            show={this.state.smShow}
            onHide={this.smClose}
            aria-labelledby="example-modal-sizes-title-sm"
            >
            <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-sm">
                    {this.props.title}
                </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.props.children}
                </Modal.Body>
            </Modal>
        )
    }
}

export default MeuModal;