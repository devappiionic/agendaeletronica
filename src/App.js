import React, { Component } from 'react';
import Builder from './containers/Builder/Builder';

class App extends Component {
  render() {
    return (  
      <Builder />      
    );
  }
}

export default App;
