import React, {Component} from 'react';
import ContatoForm from '../../components/contatos/contato/ContatoForm';
import MeuModal from '../../components/layout/modal/MeuModal';
import Auxi from '../../helper/ui/Auxi';
import axios from 'axios';


class BuilderContatoEdicao extends Component {

    constructor(props){
        super(props);    
        
        this.state = {
            validated: false,
            contato_save: false,
            contato_edicao: {
                id: '',
                name: '',
                sobrenome: '',
                ddd: '',
                telefone: ''                
            }
        }
    }

    componentDidMount(){

        if(this.props.location.params){
            axios.get(`http://127.0.0.1:8000/api/Contato/${this.props.location.params}`)
            .then((response) => {            
                this.setState({
                    contato_edicao: response.data
                })
            });
        }
    }

    handleCamposEdicao = (e) => {

        const campos = this.state.contato_edicao;
        let campos_array = [];
        
        Object.keys(campos)
        .map((campo) => {
            campos_array[campo] = campos[campo]
        })

        campos_array[e.target.name] = e.target.value;

        this.setState({
            contato_edicao: {...campos_array}
        });
    }

    handleSubmit = (e) => {
        
        const form = e.currentTarget;
        e.preventDefault();

        if(form.checkValidity() === false){            
            e.stopPropagation();

        }else{

            let dados_array = [];
            for(let key in this.state){
                dados_array.push({
                    id: key,
                    data: this.state[key]
                })
            }
            
            //REMOVER OS CAMPOS DESNECESSARIOS DO STATE
            const dados_filter_array = dados_array.filter(campos => {
                return campos.id == 'contato_edicao'
            });

            const dados_new_array = [];
            dados_filter_array.map(itens => {
                dados_new_array[itens.id] = itens.data;
            })

            const dados = {
                ...dados_new_array
            };
            
            if(dados.contato_edicao.id > 0){
                axios.post('http://127.0.0.1:8000/api/UpdateContato', dados.contato_edicao )
                .then(response => {                    
                    if(response.data.id > 0){
                        this.setState({
                            contato_save: true,
                            validated: true
                        })
                    }
                });
            }else{
                axios.post('http://127.0.0.1:8000/api/NovoContato', dados.contato_edicao )
                .then(response => {
                    if(response.data.id > 0){
                        this.setState({
                            contato_save: true,
                            validated: true
                        })
                    }
                });
            }
        }
    }

    handleResetForm = () => {
        const campos = this.state.contato_edicao;        
        let campos_array = [];
        
        for(let campo in campos){
            campos_array[campo] = '';
        }

        this.setState({
            contato_edicao: {...campos_array},
            validated: false,
            contato_save: false
        })
    }

    render(){        
        const val = this.state.validated;
        console.log(this.state.contato_save);
        return (      
            <Auxi>
            {this.state.contato_save ? <MeuModal title='Cadastro de Contatos' onHideModal={this.handleResetForm}>Parabens Dados Gravados com Sucesso!</MeuModal>: ''}
            <ContatoForm 
                contato={this.state.contato_edicao} 
                onChangeCampo={this.handleCamposEdicao} 
                onSubmitForm={this.handleSubmit}
                validated={val}
            />   
            </Auxi>         
        )
    }
}

export default BuilderContatoEdicao;