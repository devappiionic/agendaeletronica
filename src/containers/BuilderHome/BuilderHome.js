import React from 'react';
import { CardGroup, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class BuilderHome extends React.Component {

    constructor(props){
        super(props);        
    }

    render(){
        return(
            <CardGroup>
                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="imgs/contacts-1-512.png" style={{width: '100px'}} />
                <Card.Body>
                    <Card.Title>Formulário: Cadastro de Contatos</Card.Title>
                    <Card.Text>
                        Formulário Simples, com dados de Nome, Sobrenome e Telefone
                    </Card.Text>
                    <Link variant="primary" to="/edicao">Acessar</Link>
                </Card.Body>
                </Card>
                <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="imgs/27737-200.png" style={{width: '100px'}}/>
                <Card.Body>
                    <Card.Title>Listagem de Contatos</Card.Title>
                    <Card.Text>
                        Todos os contatos cadastrados ficam disponvieis nesta listagem
                    </Card.Text>
                    <Link variant="primary" to="/listagem" >Acessar</Link>
                </Card.Body>
                </Card>
            </CardGroup>
        );
    }
}

export default BuilderHome;