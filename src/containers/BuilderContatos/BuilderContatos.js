import React, { Component } from 'react';
import Contatos from '../../components/contatos/Contatos';
import ListContatos from '../../helper/data/datateste';
import axios from 'axios';

class BuilderContatos extends Component {

    constructor(props){
        super(props);

        this.state = {
            contatos: ListContatos
        }        
    }

    componentDidMount()
    {        
        axios.get('http://127.0.0.1:8000/api/Contatos')
        .then(response => {            
            this.setState({
                contatos: response.data
            })            
        })
    }

    render(){
        console.log(this.state.contatos);
        return (                        
            <Contatos lista={this.state.contatos} />            
        )
    }
}

export default BuilderContatos;