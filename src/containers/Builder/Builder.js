import React, {Component} from 'react';
import Layout from '../../components/layout/Layout';
import ListContatos from '../../helper/data/datateste';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BuilderContatos from '../BuilderContatos/BuilderContatos';
import BuilderContatoEdicao from '../BuilderContatoEdicao/BuilderContatoEdicao';
import BuilderHome from '../BuilderHome/BuilderHome';

class Builder extends Component {

    constructor(props){
        super(props);

        this.state = {
            contatos: ListContatos
        }        
    }

    render(){
        return (
            <BrowserRouter>
                <Layout props={this.props}>   
                    <Route path='/listagem' render={(props) => <BuilderContatos lista={this.state.contatos} {...props} />} />
                    <Route path='/edicao' component={BuilderContatoEdicao} />
                    <Route path='/home' component={BuilderHome} />                    
                </Layout>
            </BrowserRouter>
        )
    }
}

export default Builder;